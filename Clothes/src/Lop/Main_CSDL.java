package Lop;

import java.util.ArrayList;
import java.util.HashMap;

import android.widget.SimpleAdapter;

//chứa các dữ liệu trong quá trình hoạt động của app
public class Main_CSDL {
	/**Gợi ý activity*/
	//hot
	ArrayList<SanPham> sp_goiy_hot;
//	ArrayList<HashMap<String, String>> array_goiy_hot;
//	SimpleAdapter adapter_goiy_hot;
	
	//mới
	ArrayList<SanPham> sp_goiy_moi;
//	ArrayList<HashMap<String, String>> array_goiy_moi;
//	SimpleAdapter adapter_goiy_moi;
//	
	//khuyến mãi
	ArrayList<SanPham> sp_goiy_khuyenmai;
//	ArrayList<HashMap<String, String>> array_goiy_khuyenmai;
//	SimpleAdapter adapter_goiy_khuyenmai;
	
	/**Giỏ hàng*/
	ArrayList<SanPham> sp_giohang;
//	ArrayList<HashMap<String, String>> array_giohang;
//	SimpleAdapter adapter_giohang;
	
	public boolean is_dangnhap;
	ArrayList<SanPham> sanpham;
	ArrayList<NguoiDung> nguoidung;
	
	ArrayList<SanPham> sp_kq_timkiem;
	public Main_CSDL(SanPham []sp, NguoiDung []nd){
		is_dangnhap=false;
		sanpham = new ArrayList<SanPham>();
		nguoidung = new ArrayList<NguoiDung>();
		this.sp_giohang= new ArrayList<SanPham>();
		for(int i=0; i<sp.length; i++){
			this.sanpham.add(sp[i]);
			this.sp_giohang.add(sp[i]);
		}
		for(int i=0; i<nd.length; i++){
			this.nguoidung.add(nd[i]);
		}
		this.sp_goiy_hot=this.sanpham;
		this.sp_goiy_moi=this.sanpham;
		this.sp_goiy_khuyenmai=this.sanpham;
		this.sp_kq_timkiem=this.sanpham;
		
	}
	//get hot
	public ArrayList<SanPham> get_sp_goiy_hot(){return sp_goiy_hot;}
//	public ArrayList<HashMap<String, String>> get_array_goiy_hot(){return this.array_goiy_hot;}
//	public SimpleAdapter get_adapter_goiy_hot(){return adapter_goiy_hot;}
	//get moi
	public ArrayList<SanPham> get_sp_goiy_moi(){return sp_goiy_moi;}
//	public ArrayList<HashMap<String, String>> get_array_goiy_moi(){return this.array_goiy_moi;}
//	public SimpleAdapter get_adapter_goiy_moi(){return adapter_goiy_moi;}
//	//get khuyenmai
	public ArrayList<SanPham> get_sp_goiy_khuyenmai(){return sp_goiy_khuyenmai;}
//	public ArrayList<HashMap<String, String>> get_array_goiy_khuyenmai(){return this.array_goiy_khuyenmai;}
//	public SimpleAdapter get_adapter_goiy_khuyenmai(){return adapter_goiy_khuyenmai;}
	
	//giỏ hàng
	public ArrayList<SanPham> get_sp_giohang(){return sp_giohang;}
//	public ArrayList<HashMap<String, String>> get_array_giohang(){return this.array_giohang;}
//	public SimpleAdapter get_adapter_giohang(){return adapter_giohang;}
	public ArrayList<SanPham> get_sp_kq_timkiem(){return this.sp_kq_timkiem;}
	public void set_sp_goiy_hot(ArrayList<SanPham> sp){this.sp_goiy_hot=sp;}
	
	public ArrayList<SanPham> getTatCaSanPham(){return sanpham;}
	public ArrayList<NguoiDung> getNguoiDung(){
		return nguoidung;
	}
	public void set_sp_kq_timkiem(ArrayList<SanPham> sp){sp_kq_timkiem=sp;}
	
	public static Main_CSDL main_csdl = new Main_CSDL(CSDL_demo.sp_demo, CSDL_demo.nd_demo);
}
