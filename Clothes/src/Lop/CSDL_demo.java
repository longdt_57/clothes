package Lop;

import java.util.ArrayList;
import java.util.Date;

public class CSDL_demo {
	public static final SanPham []sp_demo= {
		new SanPham("1", "Quần bò nữ", "Quần", "Hà Nội", "Viettien", 220.000, "đẹp", "	114 Quán Thánh - Ba Đình Quận Ba Đình, Hà Nội", 
				"quanbonu", 4,new Date(2015, 4, 7), 0),
		new SanPham("2", "Áo Mu", "Áo", "Hà Nội", "Viettien", 200, "đủ size", "	114 Quán Thánh - Ba Đình Quận Ba Đình, Hà Nội", 
				"aomu", 3,new Date(2015, 4, 6), 20),
		new SanPham("3", "Sơ mi nam", "Áo", "Hà Nội", "Viettien", 300, "Chất lượng tốt", "	114 Quán Thánh - Ba Đình Quận Ba Đình, Hà Nội", 
				"aosominam", 5,new Date(2015, 4, 8), 10),
		new SanPham("4", "Sơ mi trắng", "Áo", "Hà Nội", "Viettien", 350, "đẹp", "	114 Quán Thánh - Ba Đình Quận Ba Đình, Hà Nội", 
				"somitrang", 4,new Date(2015, 4, 9), 0),
		new SanPham("5", "Sơ mi chỉ nhỏ", "Áo", "Hà Nội", "Viettien", 380, "chất lượng cao", "	114 Quán Thánh - Ba Đình Quận Ba Đình, Hà Nội", 
				"somichinho", 4,new Date(2015, 4, 10), 0),
		new SanPham("6", "Quần tây 27", "Quần", "TP HCM", "Canifa", 400, "đẹp", "	114 Quán Thánh - Ba Đình Quận Ba Đình, TP HCM", 
				"quantay27", 4,new Date(2015, 4, 5), 20),
		new SanPham("7", "Quần tây 54", "Quần", "TP HCM", "Canifa", 400, "đẹp", "	114 Quán Thánh - Ba Đình Quận Ba Đình, TP HCM", 
				"quantay54", 5,new Date(2015, 4, 5), 30),
		new SanPham("8", "Quần tây 58", "Quần", "TP HCM", "Canifa", 400, "đẹp", "	114 Quán Thánh - Ba Đình Quận Ba Đình, TP HCM", 
				"quantay58", 4,new Date(2015, 4, 6), 0),
				
		new SanPham("9", "Sơ mi 37", "Áo", "TP HCM", "Canifa", 280, "đẹp", "	114 Quán Thánh - Ba Đình Quận Ba Đình, TP HCM", 
				"somi37", 3,new Date(2015, 4, 5), 50),
		new SanPham("10", "Sơ mi 97", "Áo", "TP HCM", "Canifa", 320, "đẹp", "	114 Quán Thánh - Ba Đình Quận Ba Đình, TP HCM", 
						"somi37", 3,new Date(2015, 5, 1), 40),
					
	};
	public static final NguoiDung []nd_demo={
		new NguoiDung("longdt", "1234", "Long", "longdt57@gmail.com", "01697646159", "Hà Nội"),
		new NguoiDung("thangnq", "1234", "Thang", "thangnq_57@vnu.edu.vn", "", "Hà Nội"),
		new NguoiDung("luannt", "1234", "Luân", "luanng_57@vnu.edu.vn", "", "Vĩnh Phúc")
	};
}
