package Lop;

public class NguoiDung {
	String taikhoan;
	String matkhau;
	String ten;
	String email;
	String dienthoai;
	String diachi;
	
	public NguoiDung(String tk, String mk, String ten, String em, String dt, String dch){
		this.taikhoan=tk;
		this.matkhau=mk;
		this.ten=ten;
		this.email=em;
		this.dienthoai=dt;
		this.diachi=dch;
	}
	public String getTaikhoan(){return taikhoan;}
	public String getMatkhau(){return matkhau;}
	public NguoiDung clone(){
		return new NguoiDung(taikhoan, matkhau, ten, email, dienthoai, diachi);
	}
	public String getEmail(){return email;}
}
