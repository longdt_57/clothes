package Lop;
import java.util.ArrayList;
import java.util.Date;

import android.media.Rating;
import android.provider.Telephony.Mms.Rate;


public class SanPham {
	String masp;		//mã để phân biệt sp
	String ten;		//tên sản phẩm
	String loaisanpham;		//loại sản phẩm
	String khuvuc;			//khu vực
	String nhanhieu;		//nhãn hiệu
	double giaban;			//giá bán
	String nguoiban;	//người bán
	
	String noidung;			//mo ta chi tiết sản phẩm
	String lienhe;			//liên hệ
	String anh;			//link hình ảnh cho sản phẩm
	
	int rate;			//rating
	Date ngaydang; 			//ngày đăng
	int khuyenmai;			//khuyến mại
	
	public SanPham(String ten, String anh){
		this.ten=ten;
		this.anh=anh;
	}
	public String getAnh(){return anh;}
	public double getGiaBan(){return giaban;}
	public String getLoaiSanPham(){return loaisanpham;}
	public String getKhuVuc(){return khuvuc;}
	public String getNhanHieu(){return nhanhieu;}
	public String getTen(){return ten;}
	public Date getNgaydang(){return this.ngaydang;}
	public int getKhuyenmai(){return this.khuyenmai;}
	public String getNoidung(){return this.noidung;}
	public String getLienhe(){return this.lienhe;}
	public int getRate(){return this.rate;}
	public SanPham(String msp, String ten, String lsp, String kv, String nh, double gb, String nd, String lh, 
			String anh,int r, Date ngaydang, int km){
		this.masp=msp;
		this.ten = ten;
		this.loaisanpham=lsp;
		this.khuvuc=kv;
		this.nhanhieu=nh;
		this.giaban=gb;
		this.noidung=nd;
		this.lienhe=lh;
		this.anh=anh;
		this.rate=r;
		this.ngaydang=ngaydang;
		this.khuyenmai=km;
	}
	public SanPham clone(){
		return new SanPham(masp, ten, loaisanpham, khuvuc, nhanhieu, giaban, noidung, lienhe, anh, rate, ngaydang, khuyenmai);
	}
	
}
