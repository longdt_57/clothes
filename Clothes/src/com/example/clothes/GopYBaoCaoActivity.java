package com.example.clothes;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GopYBaoCaoActivity extends Activity{
	ActionBar actionBar;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gopybaocao);
		
		
		MainActivity tabSample = (MainActivity)getParent();
		actionBar = tabSample.getActionBar();
		actionBar.setTitle("Góp ý-Báo cáo");
		
		//gửi mail
		Button bt_gui= (Button) findViewById(R.id.bt_gybc_gui);
		bt_gui.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
				email.setType("message/rfc822");
				email.putExtra(Intent.EXTRA_EMAIL,getResources().getString(R.string.mailto));
				
				EditText subject	= (EditText) findViewById(R.id.et_gybc_tieude);
				EditText body	=	(EditText) findViewById(R.id.et_gybc_noidung);
				
				email.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
				email.putExtra(Intent.EXTRA_TEXT, body.getText().toString());
				try {
					        // the user can choose the email client
					         startActivity(Intent.createChooser(email, "Choose an email client from..."));
					      
				} catch (android.content.ActivityNotFoundException ex) {
					    	  Toast.makeText(GopYBaoCaoActivity.this, "No email client installed.",
					    			                   Toast.LENGTH_LONG).show();
				}
				subject.setText("");
				body.setText("");

			}
		});
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		actionBar.setTitle("Góp ý-Báo cáo");
		super.onResume();
	}

}
