package com.example.clothes;

import java.util.ArrayList;
import java.util.HashMap;

import Lop.Main_CSDL;
import Lop.SanPham;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TimKiemActivity extends Activity{
	ActionBar actionBar;
	Spinner sn_loaisanpham;
	Spinner sn_khuvuc;
	Spinner sn_nhanhieu;
	Spinner sn_khoanggia;
	ImageView iv_search;
	EditText et_tim;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timkiem);
		
		MainActivity tabSample = (MainActivity)getParent();
		actionBar = tabSample.getActionBar();
		actionBar.setTitle("Tìm kiếm");
		
		taoluachon();
		iv_search = (ImageView) findViewById(R.id.iv_timkiem);
		iv_search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//set listview ket qua tim kiem
				et_tim=(EditText) findViewById(R.id.et_timkiem);
				
				String ten=et_tim.getText().toString();
				String loai=sn_loaisanpham.getSelectedItem().toString();
				String khuvuc=sn_khuvuc.getSelectedItem().toString();
				String nhanhieu=sn_nhanhieu.getSelectedItem().toString();
				String khoanggia=sn_khoanggia.getSelectedItem().toString();
				ArrayList<SanPham>tmp;			
				tmp=timkiem(Main_CSDL.main_csdl.getTatCaSanPham(),ten,loai,
						khuvuc,nhanhieu,khoanggia);
				Main_CSDL.main_csdl.set_sp_kq_timkiem(tmp);
				
				ListView listview_kq = (ListView) findViewById(R.id.lv_timkiem);
				setlist(listview_kq, Main_CSDL.main_csdl.get_sp_kq_timkiem());
			}
		});
		
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		actionBar.setTitle("Tìm kiếm");
		super.onResume();
	}
	
	private void taoluachon(){
		//thêm list cho loại sản phẩm
				sn_loaisanpham = (Spinner) findViewById(R.id.sn_loaisanpham);
				ArrayAdapter<CharSequence> aa_loaisanpham= ArrayAdapter.createFromResource(
						this, R.array.timkiem_loaisanpham, R.layout.spinner_item);
				aa_loaisanpham.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sn_loaisanpham.setAdapter(aa_loaisanpham);
				
				//thêm list cho khu vực
				sn_khuvuc= (Spinner) findViewById(R.id.sn_khuvuc);
				ArrayAdapter<CharSequence> aa_khuvuc = ArrayAdapter.createFromResource(
						this,  R.array.timkiem_khuvuc, R.layout.spinner_item);
				aa_khuvuc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sn_khuvuc.setAdapter(aa_khuvuc);
				
				//thêm list cho nhãn hiệu
				sn_nhanhieu= (Spinner) findViewById(R.id.sn_nhanhieu);
				ArrayAdapter<CharSequence> aa_nhanhieu = ArrayAdapter.createFromResource(
						this,  R.array.timkiem_nhanhieu, R.layout.spinner_item);
				aa_nhanhieu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sn_nhanhieu.setAdapter(aa_nhanhieu);
				
				//thêm list cho giá
				sn_khoanggia= (Spinner) findViewById(R.id.sn_khoanggia);
				ArrayAdapter<CharSequence> aa_khoanggia = ArrayAdapter.createFromResource(
						this,  R.array.timkiem_khoanggia, R.layout.spinner_item);
				aa_khoanggia.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sn_khoanggia.setAdapter(aa_khoanggia);
	}
	private void setlist(ListView lv, ArrayList<SanPham> sp){
		String IMAGE = "Image";
		String NAME = "Name";
		String GIA= "Gia";
		ArrayList<HashMap<String, String>> array= new ArrayList<HashMap<String, String>>();
		for(int i=0; i<sp.size(); i++){
			HashMap<String, String> temp= new HashMap<String, String>();
			
			//xu ly anh sang int
			String str_tmp= sp.get(i).getAnh();
				String uri = "drawable/"+ str_tmp; 
				int imageResource = getResources().getIdentifier(uri, null, getPackageName());

			temp.put(IMAGE, String.valueOf(imageResource));
			temp.put(NAME, sp.get(i).getTen());
			temp.put(GIA, (Double.toString( sp.get(i).getGiaBan()))+" "+getResources().getString(R.string.tiente));
			array.add(temp);
		}
		String[] from = { IMAGE, NAME, GIA};
		int[] to = { R.id.iv_lv_sanpham_anh, R.id.tv_lv_sanpham_ten , R.id.tv_lv_sanpham_gia};
		SimpleAdapter adapter = new SimpleAdapter(this, array, R.layout.item_listview_sanpham, from, to);
		lv.setAdapter(adapter);
		onclick(lv, sp);
	}
	private void onclick(ListView hlv, final ArrayList<SanPham> sp){
		//hlv.setClickable(true);
		hlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int position, long arg3){
				ImageView iv_anh=(ImageView) arg1.findViewById(R.id.iv_lv_sanpham_anh);
				iv_anh.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						ChiTiet_SanPham.sanpham=sp.get(position);
						Intent it_goiy_item = new Intent(TimKiemActivity.this,ChiTiet_SanPham.class);
						startActivity(it_goiy_item);
					}
				});
				ImageView iv_giohang = (ImageView) arg1.findViewById(R.id.iv_item_lv_sanpham_giohang);
				iv_giohang.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Main_CSDL.main_csdl.get_sp_giohang().add(Main_CSDL.main_csdl.get_sp_kq_timkiem().get(position));
						Toast toast=Toast.makeText(TimKiemActivity.this, "Đã thêm vào giỏ hàng",  Toast.LENGTH_SHORT);
						toast.show();
						
					}
				});
					
			}
		});
	}
	
	//ham xử lý tìm kiếm
	protected ArrayList<SanPham> timkiem(final ArrayList<SanPham> sp_in,String ten, String loai, 
			String khuvuc, String nhanhieu,String khoanggia){
		ArrayList<SanPham> sp_out = new ArrayList<SanPham>();
		String []arr1=getResources().getStringArray(R.array.timkiem_loaisanpham);
		String []arr2=getResources().getStringArray(R.array.timkiem_khuvuc);
		String []arr3=getResources().getStringArray(R.array.timkiem_nhanhieu);
		String []arr4=getResources().getStringArray(R.array.timkiem_khoanggia);
		
		for(int i=0; i<sp_in.size(); i++){
			SanPham tmp= sp_in.get(i);
			if(ten.compareTo("")==0||tmp.getTen().compareTo(ten)==0){
				if(loai.compareTo(arr1[0])==0||tmp.getLoaiSanPham().compareTo(loai)==0) {
					if(khuvuc.compareTo(arr2[0])==0||tmp.getKhuVuc().compareTo(khuvuc)==0) {
						if(nhanhieu.compareTo(arr3[0])==0||tmp.getNhanHieu().compareTo(nhanhieu)==0) {
							if(khoanggia.compareTo(arr4[0])==0) {
								sp_out.add(sp_in.get(i));
							}else {
								String []arr5=khoanggia.split("-");
								double min=Double.parseDouble(arr5[0]);
								double max=Double.parseDouble(arr5[1]);
								double gia=tmp.getGiaBan();
								if(gia>=min&&gia<=max) sp_out.add(sp_in.get(i));
							}
						}
					}
				}
			}
			
		}
		
		return sp_out;
		
	}
	
}
