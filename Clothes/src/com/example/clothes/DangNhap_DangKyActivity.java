package com.example.clothes;

import java.util.ArrayList;

import Lop.Main_CSDL;
import Lop.NguoiDung;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DangNhap_DangKyActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dangnhap_dangky);
		
		ActionBar actionBar =	getActionBar();
		actionBar.setTitle("Đăng ký");
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		Button bt_dangky= (Button) findViewById(R.id.bt_dangky_dangky);
		bt_dangky.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String str_taikhoan = ((EditText) findViewById(R.id.et_dangky_taikhoan)).getText().toString();
				String str_matkhau = ((EditText) findViewById(R.id.et_dangky_matkhau)).getText().toString();
				String str_nlmk = ((EditText) findViewById(R.id.et_dangky_nlmatkhau)).getText().toString();
				String str_ten = ((EditText) findViewById(R.id.et_dangky_ten)).getText().toString();
				String str_email = ((EditText) findViewById(R.id.et_dangky_email)).getText().toString();
				String str_dt = ((EditText) findViewById(R.id.et_dangky_dienthoai)).getText().toString();
				String str_dch = ((EditText) findViewById(R.id.et_dangky_diachi)).getText().toString();
				
				if(str_taikhoan.compareTo("")==0||str_matkhau.compareTo("")==0||str_email.compareTo("")==0){
					Toast toast=Toast.makeText(DangNhap_DangKyActivity.this, "Điền đủ Tài khoản, mật khẩu hoặc email!",  Toast.LENGTH_SHORT);
					toast.show();
				}else{
					ArrayList<NguoiDung> tmp = Main_CSDL.main_csdl.getNguoiDung();
					boolean is_tontai=false;
					for(int i=0; i<tmp.size(); i++){
						if(tmp.get(i).getTaikhoan().compareTo(str_taikhoan)==0){
							Toast toast=Toast.makeText(DangNhap_DangKyActivity.this, "Tên tài khoản đã được sử dụng!",  Toast.LENGTH_SHORT);
							toast.show();
							is_tontai=true;
							break;
						}
					}
					if(!is_tontai){
						NguoiDung nd_tmp= new NguoiDung(str_taikhoan, str_matkhau, str_ten, str_email, str_dt, str_dch);
						tmp.add(nd_tmp);
						Toast toast = Toast.makeText(DangNhap_DangKyActivity.this, "Đăng ký thành công!", Toast.LENGTH_LONG);
						toast.show();
						finish();
					}
					
				}
				
			}
		});
	}
}
