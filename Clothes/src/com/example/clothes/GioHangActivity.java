package com.example.clothes;

import java.util.ArrayList;
import java.util.HashMap;

import Lop.Main_CSDL;
import Lop.SanPham;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class GioHangActivity extends Activity{
	ListView listview_kq;
	ArrayList<HashMap<String, String>> array;
	SimpleAdapter adapter;
	ActionBar actionBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_giohang);
		
		MainActivity tabSample = (MainActivity)getParent();
		actionBar = tabSample.getActionBar();
		actionBar.setTitle("Giỏ hàng");
		
		listview_kq = (ListView) findViewById(R.id.lv_giohang);
		setlist(listview_kq, Main_CSDL.main_csdl.get_sp_giohang());
	}
	
	private void setlist(ListView lv, ArrayList<SanPham> sp){
		String IMAGE = "Image";
		String NAME = "Name";
		String GIA= "Gia";
		String IMAGE2 = "Image2";
		array= new ArrayList<HashMap<String, String>>();
		for(int i=0; i<sp.size(); i++){
			HashMap<String, String> temp= new HashMap<String, String>();
			
			//xu ly anh sang int
			String str_tmp= sp.get(i).getAnh();
				String uri = "drawable/"+ str_tmp; 
				int imageResource = getResources().getIdentifier(uri, null, getPackageName());

			temp.put(IMAGE, String.valueOf(imageResource));
			temp.put(NAME, sp.get(i).getTen());
			temp.put(GIA, (Double.toString( sp.get(i).getGiaBan()))+" "+getResources().getString(R.string.tiente));
			temp.put(IMAGE2,String.valueOf(R.drawable.ic_bin));
			array.add(temp);
		}
		String[] from = { IMAGE, NAME, GIA, IMAGE2};
		int[] to = { R.id.iv_lv_sanpham_anh, R.id.tv_lv_sanpham_ten , R.id.tv_lv_sanpham_gia, R.id.iv_item_lv_sanpham_giohang};
		adapter = new SimpleAdapter(this, array, R.layout.item_listview_sanpham, from, to);
		lv.setAdapter(adapter);
		onclick(lv, sp);
	}
	private void onclick(final ListView hlv, final ArrayList<SanPham> sp){
		//hlv.setClickable(true);
		hlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, final View arg1, final int position, long arg3){
				ImageView iv_anh=(ImageView) arg1.findViewById(R.id.iv_lv_sanpham_anh);
				iv_anh.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						ChiTiet_SanPham.sanpham=sp.get(position);
						Intent it_goiy_item = new Intent(GioHangActivity.this,ChiTiet_SanPham.class);
						startActivity(it_goiy_item);
					}
				});
				ImageView iv_bin = (ImageView) arg1.findViewById(R.id.iv_item_lv_sanpham_giohang);
				iv_bin.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//hlv.removeViewAt(position);
						Main_CSDL.main_csdl.get_sp_giohang().remove(position);
						array.remove(position);
						adapter.notifyDataSetChanged();
					}
				});
				
				
				
			}
		});
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		actionBar.setTitle("Giỏ hàng");
		setlist(listview_kq, Main_CSDL.main_csdl.get_sp_giohang());
		super.onResume();
	}

}
