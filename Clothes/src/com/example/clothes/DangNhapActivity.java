package com.example.clothes;

import java.util.ArrayList;

import Lop.Main_CSDL;
import Lop.NguoiDung;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DangNhapActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dangnhap);
		
		ActionBar atb = getActionBar();
		atb.setTitle("Đăng Nhập");
		atb.setDisplayHomeAsUpEnabled(true);
		
		
		//thiet lap nut dang ky
		Button bt_dangky = (Button) findViewById(R.id.bt_dangnhap_dangky);
		bt_dangky.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myIntent=new Intent(DangNhapActivity.this, DangNhap_DangKyActivity.class);
				startActivity(myIntent);
			}
		});
		
		//thiết lập nút quên mật khẩu
		Button bt_quenmk = (Button) findViewById(R.id.bt_dangnhap_quenmatkhau);
		bt_quenmk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent myIntent2 = new Intent(DangNhapActivity.this, DangNhap_QuenMKActivity.class);
				startActivity(myIntent2);
			}
		});
		
		//thiết lập nút đăng nhập
		Button bt_dangnhap = (Button) findViewById(R.id.bt_dangnhap_dangnhap);
		bt_dangnhap.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EditText et_taikhoan= (EditText) findViewById(R.id.et_dangnhap_taikhoan);
				String str_taikhoang=et_taikhoan.getText().toString();
				EditText et_matkhau = (EditText) findViewById(R.id.et_dangnhap_matkhau);
				String str_matkhau=et_matkhau.getText().toString();
				if(et_matkhau.getText().toString().compareTo("")==0||et_taikhoan.getText().toString().compareTo("")==0){
					Toast toast=Toast.makeText(DangNhapActivity.this, "Bạn chưa điền tài khoản hoặc mật khẩu!",  Toast.LENGTH_SHORT);
					toast.show();
				}else{
					ArrayList<NguoiDung> tmp = Main_CSDL.main_csdl.getNguoiDung();
					boolean is_dangnhap=false;
					for(int i=0; i<tmp.size(); i++){
						if(tmp.get(i).getTaikhoan().compareTo(str_taikhoang)==0&&tmp.get(i).getMatkhau().compareTo(str_matkhau)==0){
							Toast toast=Toast.makeText(DangNhapActivity.this, "Đăng nhập thành công!",  Toast.LENGTH_SHORT);
									toast.show();
							is_dangnhap=true;
							Intent it_trangcanhan = new Intent(DangNhapActivity.this, DangNhap_TrangCaNhanActivity.class);
							startActivity(it_trangcanhan);
							break;
						}
						
					}
					if(!is_dangnhap){
						Toast toast=Toast.makeText(DangNhapActivity.this, "Sai tài khoản hoặc mật khẩu!",  Toast.LENGTH_SHORT);
						toast.show();
					}
				}
			}	
		});
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
		    case android.R.id.home:
		        finish();
	    }
	    return super.onOptionsItemSelected(item);
	}
}
