package com.example.clothes;
import java.util.ArrayList;

import Lop.Main_CSDL;
import Lop.NguoiDung;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;
import javax.activation.*;



public class DangNhap_QuenMKActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dangnhap_quenmatkhau);
		
		ActionBar actionBar =	getActionBar();
		actionBar.setTitle("Quên mật khẩu");
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		Button bt_gui = (Button) findViewById(R.id.bt_dangnhap_quenmatkhau_gui);
		bt_gui.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String str_taikhoan = ((EditText) findViewById(R.id.et_quenmatkhau_taikhoan)).getText().toString();
				String str_email = ((EditText) findViewById(R.id.et_quenmatkhau_email)).getText().toString();
				if(str_taikhoan.compareTo("")==0||str_email.compareTo("")==0){
					Toast toast = Toast.makeText(DangNhap_QuenMKActivity.this, "Tài khoản hoặc email vẫn trống!", 
							Toast.LENGTH_SHORT);
					toast.show();
				}else{
					ArrayList<NguoiDung> tmp = Main_CSDL.main_csdl.getNguoiDung();
					boolean is_gui=false;
					for(int i=0; i<tmp.size(); i++){
						if(tmp.get(i).getTaikhoan().compareTo(str_taikhoan)==0&&tmp.get(i).getEmail().compareTo(str_email)==0){
							
							//gửi mail
							DangNhap_QuenMKActivity.sendFromGMail("longdt57@gmail.com", "yeuem1508",
									tmp.get(i).getEmail(), "Cấp lại mật khẩu", tmp.get(i).getMatkhau());
							Toast toast = Toast.makeText(DangNhap_QuenMKActivity.this, "Đã gửi", Toast.LENGTH_LONG);
							toast.show();
							is_gui=true;
							finish();
							break;
						}
					}
					if(!is_gui){
						Toast toast = Toast.makeText(DangNhap_QuenMKActivity.this,
								"Tài khoản hoặc email sai!", Toast.LENGTH_LONG);
						toast.show();
					}
				}
				
			}
		});
	}
	private static void sendFromGMail(String from, String pass, String to, String subject, String body) {}
	   
}
