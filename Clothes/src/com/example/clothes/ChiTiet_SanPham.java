package com.example.clothes;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;

import Lop.Main_CSDL;
import Lop.SanPham;
import android.app.ActionBar;
import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class ChiTiet_SanPham extends Activity{

	public static SanPham sanpham;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.goiy_item);
		
		ActionBar actionBar =	getActionBar();
		actionBar.setTitle("Chi tiết");
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		TextView tv_ngaydang= (TextView) findViewById(R.id.tv_goiy_item_ngaydang);
		TextView tv_gia=(TextView) findViewById(R.id.tv_goiy_item_gia);
		TextView tv_ten=(TextView)findViewById(R.id.tv_goiy_item_ten);
		TextView tv_khuyenmai=(TextView)findViewById(R.id.tv_goiy_item_khuyenmai);
		//TextView tv_noidung=(TextView)findViewById(R.id.tv_goiy_item_noidung);
		TextView tv_lienhe=(TextView)findViewById(R.id.tv_goiy_item_lienhe);
		RatingBar rt_rate= (RatingBar)findViewById(R.id.rb_goiy_item_rate);
		ImageView iv_anh=(ImageView)findViewById(R.id.iv_goiy_item_anh);
		ImageView iv_giohang=(ImageView)findViewById(R.id.iv_goiy_item_giohang);
		
		Date tmp=sanpham.getNgaydang();
		tv_ngaydang.setText(tmp.getDay()+"/"+tmp.getMonth()+"/"+tmp.getYear());
		tv_gia.setText(Double.toString(sanpham.getGiaBan())+" "+getResources().getString(R.string.tiente));
		tv_ten.setText(sanpham.getTen());
		tv_khuyenmai.setText(Integer.toString(sanpham.getKhuyenmai()));
		//tv_noidung.setText(sanpham.getNoidung());
		tv_lienhe.setText(sanpham.getLienhe());	
		rt_rate.setRating(sanpham.getRate());
		iv_giohang.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(Main_CSDL.main_csdl.get_sp_giohang().indexOf(sanpham)<0){
					Main_CSDL.main_csdl.get_sp_giohang().add(sanpham);
					Toast toast=Toast.makeText(ChiTiet_SanPham.this, "Đã thêm vào giỏ hàng",  Toast.LENGTH_SHORT);
					toast.show();
				}else{
					Toast toast=Toast.makeText(ChiTiet_SanPham.this, "Đã tồn tại trong giỏ hàng",  Toast.LENGTH_SHORT);
					toast.show();
				}
			}
		});
		
		//phóng to ảnh
		boolean zoomOut =  false;
		final String str_tmp= sanpham.getAnh();
		final String uri = "drawable/"+ str_tmp; 
		final int imageResource = getResources().getIdentifier(uri, null, getPackageName());
		iv_anh.setImageResource(imageResource);
		iv_anh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
		    case android.R.id.home:
		        finish();
	    }
	    return super.onOptionsItemSelected(item);
	}
}
