package com.example.clothes;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;

public class DangNhap_TrangCaNhanActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dangnhap_trangcanhan);
		
		ActionBar actionBar =	getActionBar();
		actionBar.setTitle("Trang cá nhân");
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

}
