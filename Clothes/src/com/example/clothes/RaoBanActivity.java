package com.example.clothes;

import java.io.FileNotFoundException;
import java.io.InputStream;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class RaoBanActivity extends Activity {
	private final int SELECT_PHOTO = 1;
	ImageView iv_sp;
	ActionBar actionBar;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_raoban);
		
		MainActivity tabSample = (MainActivity)getParent();
		actionBar = tabSample.getActionBar();
		actionBar.setTitle("Rao bán");
		
		//thêm list cho loại sản phẩm
				Spinner sn_loaisanpham = (Spinner) findViewById(R.id.sn_loaisanpham);
				ArrayAdapter<CharSequence> aa_loaisanpham= ArrayAdapter.createFromResource(
						this, R.array.timkiem_loaisanpham, R.layout.spinner_item);
				aa_loaisanpham.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sn_loaisanpham.setAdapter(aa_loaisanpham);
				
				//thêm list cho khu vực
				Spinner sn_khuvuc= (Spinner) findViewById(R.id.sn_khuvuc);
				ArrayAdapter<CharSequence> aa_khuvuc = ArrayAdapter.createFromResource(
						this,  R.array.timkiem_khuvuc, R.layout.spinner_item);
				aa_khuvuc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sn_khuvuc.setAdapter(aa_khuvuc);
				
				//thêm list cho nhãn hiệu
				Spinner sn_nhanhieu= (Spinner) findViewById(R.id.sn_nhanhieu);
				ArrayAdapter<CharSequence> aa_nhanhieu = ArrayAdapter.createFromResource(
						this,  R.array.timkiem_nhanhieu, R.layout.spinner_item);
				aa_nhanhieu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sn_nhanhieu.setAdapter(aa_nhanhieu);
				
				//pick ảnh cho sản phẩm
				iv_sp = (ImageView) findViewById(R.id.iv_raoban_anh);
				iv_sp.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent i = new Intent(Intent.ACTION_PICK);
								//android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						i.setType("image/*");
						startActivityForResult(i, SELECT_PHOTO);
					}
				});
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		actionBar.setTitle("Rao bán");
		super.onResume();
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent){
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		switch(requestCode) { 
        case SELECT_PHOTO:
            if(resultCode == RESULT_OK){
				try {
					final Uri imageUri = imageReturnedIntent.getData();
					final InputStream imageStream = getContentResolver().openInputStream(imageUri);
					final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
					iv_sp.setImageBitmap(selectedImage);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

            }
        }
	}
}
