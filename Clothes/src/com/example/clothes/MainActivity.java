package com.example.clothes;

import Lop.Main_CSDL;
import android.app.ActionBar;
import android.app.TabActivity;
import android.content.ClipData.Item;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class MainActivity extends TabActivity {

	
	public static ActionBar actionBar;
	TabHost tabHost;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//csdl demo
		
		//test database
		//SQLiteDatabase dt = SQLiteDatabase.openOrCreateDatabase("clothes.db",null);
		
		tabHost=getTabHost();
		
		//khởi tạo tab goi y
		TabSpec goiyspec=tabHost.newTabSpec("GoiY");
		//thiết lập tên tab hiển thị và icon
		goiyspec.setIndicator("",getResources().getDrawable(R.drawable.icon_goiy_tab));
		//thiết lập nội dung cho tab
		Intent goiyIntent= new Intent(this,GoiYActivity.class);
		goiyspec.setContent(goiyIntent);
		
		//khởi tạo tab tim kiem
		TabSpec timkiemspec=tabHost.newTabSpec("TimKiem");
		//thiết lập tên tab và icon
		timkiemspec.setIndicator("", getResources().getDrawable(R.drawable.icon_timkiem_tab));
		//thiết lập nội dung cho tab
		Intent timkiemIntent = new Intent(this, TimKiemActivity.class);
		timkiemspec.setContent(timkiemIntent);
		
		//khởi tạo tab rao bán
		TabSpec raobanspec = tabHost.newTabSpec("RaoBan");
		raobanspec.setIndicator("", getResources().getDrawable(R.drawable.icon_raoban_tab));
		Intent raobanIntent= new Intent(this, RaoBanActivity.class);
		raobanspec.setContent(raobanIntent);
		
		//khởi tạo tab giỏ hàng
		TabSpec giohangspec= tabHost.newTabSpec("GioHang");
		giohangspec.setIndicator("", getResources().getDrawable(R.drawable.icon_giohang_tab));
		Intent giohangIntent= new Intent(this, GioHangActivity.class);
		giohangspec.setContent(giohangIntent);
		
		//khởi tạo tab đăng nhập
//		TabSpec dangnhapspec = tabHost.newTabSpec("DangNhap");
//		dangnhapspec.setIndicator("", getResources().getDrawable(R.drawable.icon_dangnhap_tab));
//		Intent dangnhapIntent= new Intent(this, DangNhapActivity.class);
//		dangnhapspec.setContent(dangnhapIntent);
		
		//khởi tạo tab góp ý
		TabSpec gopyspec = tabHost.newTabSpec("GopYBaoCao");
		gopyspec.setIndicator("", getResources().getDrawable(R.drawable.icon_gopybaocao_tab));
		Intent gopyIntent= new Intent(this, GopYBaoCaoActivity.class);
		gopyspec.setContent(gopyIntent);
		
		
		//thêm các tabspec vào tabhost
		tabHost.addTab(goiyspec);
		tabHost.addTab(timkiemspec);
		tabHost.addTab(raobanspec);
		tabHost.addTab(giohangspec);
		//tabHost.addTab(dangnhapspec);
		tabHost.addTab(gopyspec);
		
		//thiet lập action bar
		this.actionBar = getActionBar();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
			case R.id.action_dangnhap:
				Intent it_dangnhap= new Intent(MainActivity.this, DangNhapActivity.class);
				startActivity(it_dangnhap);
				if(Main_CSDL.main_csdl.is_dangnhap==true) item.setTitle("Đăng xuất");
				else item.setTitle("Đăng nhập");
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
		
	}

}
