package com.example.clothes;

import java.util.ArrayList;
import java.util.HashMap;

import Lop.Main_CSDL;
import Lop.SanPham;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class GoiYTatCa extends Activity{
	ListView listview_kq;
	ArrayList<SanPham> sp_tatca=Main_CSDL.main_csdl.getTatCaSanPham();
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_giohang);
		
		ActionBar actionBar= getActionBar();
		actionBar.setTitle("Tất cả sản phẩm");
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		listview_kq = (ListView) findViewById(R.id.lv_giohang);
		listview_kq.getLayoutParams().height+=130;
		setlist(listview_kq,sp_tatca);
	}
	private void setlist(ListView lv, ArrayList<SanPham> sp){
		String IMAGE = "Image";
		String NAME = "Name";
		String GIA= "Gia";
		ArrayList<HashMap<String, String>> array= new ArrayList<HashMap<String, String>>();
		for(int i=0; i<sp.size(); i++){
			HashMap<String, String> temp= new HashMap<String, String>();
			
			//xu ly anh sang int
			String str_tmp= sp.get(i).getAnh();
				String uri = "drawable/"+ str_tmp; 
				int imageResource = getResources().getIdentifier(uri, null, getPackageName());

			temp.put(IMAGE, String.valueOf(imageResource));
			temp.put(NAME, sp.get(i).getTen());
			temp.put(GIA, (Double.toString( sp.get(i).getGiaBan()))+" "+getResources().getString(R.string.tiente));
			array.add(temp);
		}
		String[] from = { IMAGE, NAME, GIA};
		int[] to = { R.id.iv_lv_sanpham_anh, R.id.tv_lv_sanpham_ten , R.id.tv_lv_sanpham_gia};
		SimpleAdapter adapter = new SimpleAdapter(this, array, R.layout.item_listview_sanpham, from, to);
		lv.setAdapter(adapter);
		onclick(lv, sp);
	}
	private void onclick(ListView hlv, final ArrayList<SanPham> sp){
		//hlv.setClickable(true);
		hlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int position, long arg3){
				ImageView iv_anh=(ImageView) arg1.findViewById(R.id.iv_lv_sanpham_anh);
				iv_anh.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						ChiTiet_SanPham.sanpham=sp.get(position);
						Intent it_goiy_item = new Intent(GoiYTatCa.this,ChiTiet_SanPham.class);
						startActivity(it_goiy_item);
					}
				});
				ImageView iv_giohang = (ImageView) arg1.findViewById(R.id.iv_item_lv_sanpham_giohang);
				iv_giohang.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(Main_CSDL.main_csdl.get_sp_giohang().indexOf(sp_tatca.get(position))<0){
							Main_CSDL.main_csdl.get_sp_giohang().add(sp_tatca.get(position));
							Toast toast=Toast.makeText(GoiYTatCa.this, "Đã thêm vào giỏ hàng",  Toast.LENGTH_SHORT);
							toast.show();
						}else{
							Toast toast=Toast.makeText(GoiYTatCa.this, "Đã tồn tại trong giỏ hàng",  Toast.LENGTH_SHORT);
							toast.show();
						}
						
					}
				});
					
			}
		});
	}
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
		    case android.R.id.home:
		        finish();
	    }
	    return super.onOptionsItemSelected(item);
	}
}
