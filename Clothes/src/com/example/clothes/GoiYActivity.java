package com.example.clothes;

import java.util.ArrayList;
import java.util.HashMap;

import Lop.Main_CSDL;
import Lop.SanPham;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import com.sileria.android.view.HorzListView;

public class GoiYActivity extends Activity{
	// khai bao cac hang String can thiet cho HashMap
	ActionBar actionBar;
	HorzListView listview_hot;
	HorzListView listview_moi;
	HorzListView listview_khuyenmai;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goiy);
		
		//set title actionbar
		MainActivity tabSample = (MainActivity)getParent();
		actionBar = tabSample.getActionBar();
		actionBar.setTitle("Gợi Ý");
		
		//set listview hot
		listview_hot = (HorzListView) findViewById(R.id.hlv_hot);
		setlist(listview_hot, Main_CSDL.main_csdl.get_sp_goiy_hot());
		ImageView iv_hot = (ImageView) findViewById(R.id.iv_goiy_hot);
		onclick_all(iv_hot);
		
		//set listview moi
		listview_moi = (HorzListView) findViewById(R.id.hlv_moi);
		setlist(listview_moi, Main_CSDL.main_csdl.get_sp_goiy_moi());
		ImageView iv_moi = (ImageView) findViewById(R.id.iv_goiy_moi);
		onclick_all(iv_moi);
		
		//set listview khuyenmai
		listview_khuyenmai = (HorzListView) findViewById(R.id.hlv_khuyenmai);
		setlist(listview_khuyenmai, Main_CSDL.main_csdl.get_sp_goiy_khuyenmai());
		ImageView iv_km = (ImageView) findViewById(R.id.iv_goiy_khuyenmai);
		onclick_all(iv_km);	 
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		actionBar.setTitle("Gợi Ý");
		
		//set listview hot
		setlist(listview_hot, Main_CSDL.main_csdl.get_sp_goiy_hot());
		//set listview moi
		setlist(listview_moi, Main_CSDL.main_csdl.get_sp_goiy_moi());
		//set listview khuyenmai
		setlist(listview_khuyenmai, Main_CSDL.main_csdl.get_sp_goiy_khuyenmai());
		
		super.onResume();
	}
	private void setlist(HorzListView hlv, ArrayList<SanPham> sp){
		String IMAGE = "Image";
		String NAME = "Name";
		String GIA= "Gia";
		ArrayList<HashMap<String, String>> array= new ArrayList<HashMap<String, String>>();
		for(int i=0; i<sp.size(); i++){
			HashMap<String, String> temp= new HashMap<String, String>();
			
			//xu ly anh sang int
			String str_tmp= sp.get(i).getAnh();
				String uri = "drawable/"+ str_tmp; 
				int imageResource = getResources().getIdentifier(uri, null, getPackageName());

			temp.put(IMAGE, String.valueOf(imageResource));
			temp.put(NAME, sp.get(i).getTen());
			temp.put(GIA, (Double.toString( sp.get(i).getGiaBan())+" "+getResources().getString(R.string.tiente)));
			array.add(temp);
		}
		String[] from = { IMAGE, NAME, GIA};
		int[] to = { R.id.img_anh, R.id.tv_ten , R.id.tv_gia};
		SimpleAdapter adapter = new SimpleAdapter(this, array, R.layout.item_horizontal_listview_goiy, from, to);
		hlv.setAdapter(adapter);
		onclick(hlv, sp);
	}
	private void onclick(HorzListView hlv, final ArrayList<SanPham> sp){
		//hlv.setClickable(true);
		hlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3){
				ChiTiet_SanPham.sanpham=sp.get(position);
				Intent it_goiy_item = new Intent(GoiYActivity.this,ChiTiet_SanPham.class);
				startActivity(it_goiy_item);
				
			}
		});
	}
	private void onclick_all(ImageView iv_hot){
		iv_hot.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent it= new Intent(GoiYActivity.this, GoiYTatCa.class);
				startActivity(it);
				
			}
		});
	}
	
}
